#include <Arduino.h>
#include <IRremote.h>
#include <Adafruit_NeoPixel.h>

#define RECV_PIN 2
#define LED_PIN 13
#define STRIP_PIN 6
#define NUMPIXELS 21

IRrecv irrecv(RECV_PIN);
decode_results code;
Adafruit_NeoPixel pixels = Adafruit_NeoPixel(NUMPIXELS, STRIP_PIN, NEO_GRB + NEO_KHZ800);

unsigned long buttonUp = 0;
unsigned long buttonDown = 0;
unsigned long buttonLeft = 0;
unsigned long buttonRight = 0;
int activePixels = 0;
int red = 127, green = 140, blue = 170;
int shift = 0;

unsigned long getCode(int, String);

void setup()
{
  Serial.begin(9600);
  Serial.println("Enabling IRin");
  irrecv.enableIRIn();
  pinMode(LED_PIN, OUTPUT);

  buttonUp = getCode(3, "UP");
  buttonDown = getCode(3, "DOWN");
  buttonLeft = getCode(3, "LEFT");
  buttonRight = getCode(3, "RIGHT");
  pixels.begin(); 
}

void loop() {
  if (irrecv.decode(&code)) {

    if (code.value == buttonUp) {
      Serial.println("UP");
      activePixels++;
      if (activePixels > NUMPIXELS) {
        activePixels = NUMPIXELS;
      } 
    }

    if (code.value == buttonDown) {
      Serial.println("DOWN");
      activePixels--;
      if (activePixels < 0) {
        activePixels = 0;
      }
    }

    if (code.value == buttonLeft) {
      Serial.println("LEFT");
      shift--;
      if (shift < 0) {
        shift = 23;
      }
    }

    if (code.value == buttonRight) {
      Serial.println("RIGHT");
      shift++;
      if (shift >= 24) {
        shift = 0;
      }
    }

    for(int i=0;i<NUMPIXELS;i++){
      if (i < activePixels) {
        pixels.setPixelColor(i,(((uint32_t )64 << 24) | ((uint32_t )red << 16) | ((uint32_t )green << 8) | blue) << shift); 
      } else {
        pixels.setPixelColor(i, pixels.Color(0,0,0));
      }
      pixels.show();
    }

    Serial.println(code.value, DEC);
    irrecv.resume();
  }
  delay(300);
}

unsigned long getCode(int attempts = 3, String title = "") {
  Serial.print("Press button ");
  Serial.print(title);
  Serial.print(" ");
  Serial.print(attempts);
  Serial.println(" times");
  
  unsigned long result = 0;
  int attemptsLeft = attempts;

  while (attemptsLeft) {
    if (irrecv.decode(&code)) {
      if (result != code.value) {
        attemptsLeft = attempts;
      } else {
        attemptsLeft--;
      }
      result = code.value;
      irrecv.resume(); 
    }
  }

  digitalWrite(LED_PIN, HIGH);
  delay(100);
  digitalWrite(LED_PIN, LOW);
  return result;
} 